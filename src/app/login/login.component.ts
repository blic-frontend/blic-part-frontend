import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Booking } from '../layout/food-stall/booking';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    constructor(private router: Router, private formBuilder: FormBuilder, private loginService: LoginService) {}
    newForm: FormGroup;
    NIP: '65535';
    pass: 'admin';
    datanya: Booking[];
    single: Booking;
    ngOnInit() {
        this.newForm = new FormGroup({
            nip: new FormControl('', [
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(5)
            ]),
            pass: new FormControl('', [
                Validators.required
            ]),
        });

        this.loginService.getListBooking().subscribe(data => {
            this.datanya = data;
            console.log(this.datanya);
        });

        this.loginService.getBookingById(1).subscribe(data => {
            this.single = data;
            console.log(this.single);
        });
    }

    onLogin() {
        localStorage.setItem('isLoggedin', 'true');
        this.router.navigate(['/dashboard']);
    }
}
