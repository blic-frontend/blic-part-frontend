import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, throwError} from 'rxjs';
import { map } from 'rxjs/operators';
import { Booking } from '../layout/food-stall/booking';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) { }
  getListBooking(){
    return this.http.get<Booking[]>('http://10.1.125.184:8585/api/booking/all');
  }

  getBookingById(id: number){
    return this.http.get<Booking>('http://10.1.125.184:8585/api/booking/detail/'+id);
  }

  getBookingByTotal(){
    return this.http.get<Booking>('http://10.1.125.184:8585/api/booking/totalbook/');
  }

  getMedal(){
    return this.http.get<Booking>('http://10.1.125.184:8585/api/booking/medals/');
  }

  getFirstRank(){
    return this.http.get<Booking>('http://10.1.125.184:8585/api/booking/firstrank/');
  }

  createBooking(booking: Booking){
    return this.http.post<Booking>('http://10.1.125.184:8585/api/booking', booking);
  }
}
