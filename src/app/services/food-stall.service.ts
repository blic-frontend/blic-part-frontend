import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, throwError} from 'rxjs';
import { map } from 'rxjs/operators';
import { Login } from '../layout/dashboard/login';
import { Booking } from '../layout/food-stall/booking';
import { Stall } from '../services/stall';
@Injectable({
  providedIn: 'root'
})
export class FoodStallService {

  constructor(private http: HttpClient) { }

  getAllStalls(){
    return this.http.get<Stall[]>('http://10.1.125.184:8585/api/stall');
  }

  getStallByLocation(location: String){
    return this.http.get<Stall>('http://10.1.125.184:8585/api/stall/location/'+location);
  }

  createStall(stall: Stall){
    return this.http.post<Stall>('http://10.1.125.184:8585/api/stall', stall);
  }

  updateStall(stall: Stall){
    return this.http.post<Stall>('http://10.1.125.184:8585/api/stall', stall);
  }

  deleteStall(stall: Stall){
    return this.http.delete<Stall>('http://10.1.125.184:8585/api/stall/'+stall);
  }

  getStallId(id: Number){
    return this.http.get<Stall>('http://10.1.125.184:8585/api/stall/id');
  }

  getOneStallById(id: Number){
    return this.http.get<Stall>('http://10.1.125.184:8585/api/stall/detail/'+id);
  }

  checkStock(location: String){
    return this.http.get<Stall>('http://10.1.125.184:8585/api/stall/stock/check/'+location);
  }

  reduceStock(id: Number){
    return this.http.get<Stall>('http://10.1.125.184:8585/api/stall/stock/reduce/'+id);
  }

  resetStock(){
    return this.http.get<Stall>('http://10.1.125.184:8585/api/stall/stock/reset/');
  }
}