import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, throwError} from 'rxjs';
import { map } from 'rxjs/operators';
import { Login } from '../layout/dashboard/login';
import { Booking } from '../layout/food-stall/booking';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  //login(login: Login): Observable<any> {

    //return this.http.post('http://localhost:8585/api/credential/', login)
        //.pipe(map((res:Response) => res["content"]))
  //}

  getListBooking(){
    return this.http.get<Booking[]>('http://10.1.125.184:8585/api/booking/all');
  }

  getBookingById(id: number){
    return this.http.get<Booking>('http://10.1.125.184:8585/api/booking/detail/'+id);
  }
}
