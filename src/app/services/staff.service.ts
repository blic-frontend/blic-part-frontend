import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, throwError} from 'rxjs';
import { map } from 'rxjs/operators';
import { Login } from '../layout/dashboard/login';
import { Booking } from '../layout/food-stall/booking';
import { Staff } from '../services/staff';

@Injectable({
  providedIn: 'root'
})
export class StaffService {

  constructor(private http: HttpClient) { }
  getAllStaffs(){
    return this.http.get<Staff[]>('http://10.1.125.184:8585/api/staff/all');
  }

  getStaffById(nip: String){
    return this.http.get<Staff>('http://10.1.125.184:8585/api/staff/'+nip);
  }

  createStaff(staff: Staff){
    return this.http.post<Staff>('http://10.1.125.184:8585/api/staff/', staff);
  }

  deleteStaff(nip: String){
    return this.http.delete<Staff>('http://10.1.125.184:8585/api/staff/delete/'+nip)
  }
}
