export class Staff{
    nip: String;
    name: String;
    phone_number: String;
    gender: String; //ubah
    date: Date;
    domain: String;
    program: String;
    jabatan: String;
    internal: Boolean;
    role: String; //ubah
    flag_trainer: Boolean;
    flag_trainee: Boolean;
    created_at: Date;
    created_by: String;
    updated_at: Date;
    updated_by: String;
    is_deleted: Boolean;
    credential: String; //ubah
    division: String; //ubah
}