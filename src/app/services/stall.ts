export class Stall{
    id: Number;
    location: String;
    name: String;
    description: String;
    stock: Number;
    queue_stock: Number;
    reset_stock: Number;
    soldout_counter: Number;
    created_at: Date;
    created_by: String;
    updated_at: Date;
    updated_by: String;
    is_deleted: Boolean;
}