import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' }
];

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit {
    roleSelect: String;
    displayedColumns = ['position', 'name', 'weight', 'symbol'];
    dataSource = new MatTableDataSource(ELEMENT_DATA);
    places: Array<any> = [];
    inqBookFlag: boolean;
    inqStaffFlag: boolean;
    inqVendorFlag: boolean;
    staffFlag: boolean;
    vendorFlag: boolean;

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }
  constructor(private router: Router) { }

  ngOnInit() {
    this.roleSelect = '';
    this.inqBookFlag = false;
    this.inqStaffFlag = false;
    this.inqVendorFlag = false;
    this.staffFlag = false;
    this.vendorFlag = false;
  }

  onInqDataBook() {
    this.inqBookFlag = !this.inqBookFlag;
    this.inqStaffFlag = false;
    this.inqVendorFlag = false;
  }
  onInqDataStaff() {
    this.inqStaffFlag = !this.inqStaffFlag;
    this.inqBookFlag = false;
    this.inqVendorFlag = false;
  }  
  onInqDataVendor() {
    this.inqVendorFlag = !this.inqVendorFlag;
    this.inqBookFlag = false;
    this.inqStaffFlag = false;
  }
  onChange(roleSelect){
    this.roleSelect = roleSelect;
  }

  onSubmitUmum(){
    if(this.roleSelect === 'option1')
    {
      this.staffFlag = true;
      this.vendorFlag = false;
    }
    else if(this.roleSelect === 'option2')
    {
      this.staffFlag = false;
      this.vendorFlag = true;
    }
  }
}
