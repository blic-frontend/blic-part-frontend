import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import {   
  MatButtonModule, 
  MatCardModule,
  MatSelectModule, 
  MatIconModule, 
  MatTableModule, 
  MatRadioModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatTabsModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormsModule } from '@angular/forms';

import { AdminPageRoutingModule } from './admin-page-routing.module';
import { AdminPageComponent } from './admin-page.component';

@NgModule({
  imports: [
    CommonModule,
    AdminPageRoutingModule,
    MatRadioModule,
    MatInputModule,
    MatNativeDateModule,
    MatTabsModule,
    MatDatepickerModule,
    MatGridListModule,
    MatCardModule,
    FlexLayoutModule,
    MatSelectModule,
    MatFormFieldModule,
    MatCardModule,
    FormsModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule
  ],
  declarations: [AdminPageComponent]
})
export class AdminPageModule { }
