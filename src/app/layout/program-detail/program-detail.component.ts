import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

export class Model{
  program: String;
  description: String;
  start_date: String;
  end_date: String;
  is_deleted: number;
}

@Component({
  selector: 'app-program-detail',
  templateUrl: './program-detail.component.html',
  styleUrls: ['./program-detail.component.scss']
})
export class ProgramDetailComponent implements OnInit {

  tempStartDate: String;
  tempEndDate: String;
  tempDesc: String;
  obj: Model;
  rightIcon: string = "chevron_left";
  leftIcon: string = "create";
  flag: boolean = true;

  constructor(private dPipe: DatePipe) {
    this.obj = {
      program:'bit5', 
      description:'calon pegawai IT', 
      start_date:'1/06/2018', 
      end_date:'1/01/2019', 
      is_deleted: 0
    };
  }

  ngOnInit() {}

  noEmpty(arr){
    for(var i = 0; i<arr.length; i++){
      if(arr[i] == "" || arr[i] == null){
        arr[i] = "-";
      }
    }
  }

  changeStartDate(event, target){
    var selected_date = this.dPipe.transform(event.value, 'dd MM yyyy');
    if(target){
      console.log("set start date");
      this.tempStartDate = selected_date;
    }else{
      console.log("set end date");
      this.tempEndDate = selected_date;
    }
  }

  toggleClass(){
    document.getElementById("btnLeft").classList.toggle('btn-active-green');
    document.getElementById("btnRight").classList.toggle('btn-active-red');
  }
  rightFunction(){
    if(this.flag){
      window.history.back();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
      this.toggleClass();
    }
  }
  leftFunction(){
    if(this.flag){
      this.flag = false;
      this.leftIcon = "check";
      this.rightIcon = "close";
      this.toggleClass();
    }//else{
    //  this.updateData();
    //}
  }
  //updateData(){
  //  console.log("saved!!");
    //api here
  //}

}
