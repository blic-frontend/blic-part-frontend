import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { MatButtonModule, MatInputModule, MatIconModule, MatDatepickerModule, MatNativeDateModule} from '@angular/material';

import { ProgramDetailRoutingModule } from './program-detail-routing.module';
import { ProgramDetailComponent } from './program-detail.component';

@NgModule({
  imports: [
    CommonModule,
    ProgramDetailRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  declarations: [ProgramDetailComponent],
  providers: [DatePipe]
})
export class ProgramDetailModule { }
