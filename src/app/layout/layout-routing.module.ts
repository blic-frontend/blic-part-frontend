import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            },
            {
                path: 'components',
                loadChildren:
                    './material-components/material-components.module#MaterialComponentsModule'
            },
            {
                path: 'forms',
                loadChildren: './forms/forms.module#FormsModule'
            },
            {
                path: 'grid',
                loadChildren: './grid/grid.module#GridModule'
            },
            {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            },
            {
                path: 'blank-page',
                loadChildren: './blank-page/blank-page.module#BlankPageModule'
            },
            {
                path: 'black-page',
                loadChildren: './black-page/black-page.module#BlackPageModule'
            },
            {
                path: 'food-stall',
                loadChildren: './food-stall/food-stall.module#FoodStallModule'
            },
            {
                path: 'dashboard-vendor',
                loadChildren: './dashboard-vendor/dashboard-vendor.module#DashboardVendorModule'
            },
            {
                path: 'admin-page',
                loadChildren: './admin-page/admin-page.module#AdminPageModule'
            },
            {
                path: 'confirmation-page',
                loadChildren: './confirmation-page/confirmation-page.module#ConfirmationPageModule'
            },
            {
                path: 'staff-detail',
                loadChildren: './staff-detail/staff-detail.module#StaffDetailModule'
            },
            {
                path: 'booking-detail',
                loadChildren: './booking-detail/booking-detail.module#BookingDetailModule'
            },
            {
                path: 'program-detail',
                loadChildren: './program-detail/program-detail.module#ProgramDetailModule'
            },
            {
                path: 'schedule-detail',
                loadChildren: './schedule-detail/schedule-detail.module#ScheduleDetailModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
