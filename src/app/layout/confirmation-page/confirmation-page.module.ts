import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatIconModule } from '@angular/material';

import { ConfirmationPageRoutingModule } from './confirmation-page-routing.module';
import { ConfirmationPageComponent } from './confirmation-page.component';
import { from } from 'rxjs';

@NgModule({
  imports: [
    CommonModule,
    ConfirmationPageRoutingModule,
    MatButtonModule,
    MatIconModule
  ],
  declarations: [ConfirmationPageComponent]
})
export class ConfirmationPageModule { }
