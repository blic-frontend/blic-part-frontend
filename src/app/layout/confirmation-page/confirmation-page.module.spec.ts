import { ConfirmationPageModule } from './confirmation-page.module';

describe('ConfirmationPageModule', () => {
  let confirmationPageModule: ConfirmationPageModule;

  beforeEach(() => {
    confirmationPageModule = new ConfirmationPageModule();
  });

  it('should create an instance', () => {
    expect(confirmationPageModule).toBeTruthy();
  });
});
