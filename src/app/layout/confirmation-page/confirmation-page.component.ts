import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirmation-page',
  templateUrl: './confirmation-page.component.html',
  styleUrls: ['./confirmation-page.component.scss']
})
export class ConfirmationPageComponent implements OnInit {

  bookingID:string;
  bookingTime:string;
  bookingMenu:string;
  bookingArea:string;

  Arr = Array;
  starNumber:number = 5;
  receiveFlag:number = 1;
  starTxt:string = "star_border";

  constructor() {
    this.bookingID = "Book001";
    this.bookingTime = "23/11/2018 09:29:41";
    this.bookingMenu = "ayam gepuk";
    this.bookingArea = "lowerground";
  }

  ngOnInit() {

  }

  changeStar(num){
    var index = num+1;
    for(var i = 0; i <= num; i++){
      document.getElementById(""+i+"").innerHTML = "star";
      document.getElementById(""+i+"").style.color = "gold";
    }
  }
  changeStarBorder(){
    for(var i = 0; i <= this.starNumber; i++){
      document.getElementById(""+i+"").innerHTML = "star_border";
      document.getElementById(""+i+"").style.color = "unset";
    }
  }
  rating(index){
    var rating = index+1;
    alert("nilai yang diberikan : "+rating);
    this.receiveFlag = 1;
  }
  confirmFood(){
    this.receiveFlag = 2;
  }

}
