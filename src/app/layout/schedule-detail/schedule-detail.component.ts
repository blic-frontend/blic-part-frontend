import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

export class Model{
  tanggal: String;
  kelas: String;
  materi: String;
  program: String;
  trainer_name: String;
}

@Component({
  selector: 'app-schedule-detail',
  templateUrl: './schedule-detail.component.html',
  styleUrls: ['./schedule-detail.component.scss']
})
export class ScheduleDetailComponent implements OnInit {

  tempStartDate: String;
  tempEndDate: String;
  tempDesc: String;
  obj: Model;
  rightIcon: string = "chevron_left";
  leftIcon: string = "create";
  flag: boolean = true;
  
  trainer: String[] = ['daniel', 'setiawan', 'json', 'angular', 'typeS'];
  myControl = new FormControl({value: 'daniel', disabled: this.flag});
  trainerName: Observable<String[]>;

  constructor(private dPipe: DatePipe) {
    this.obj = {
      tanggal:'1/06/2018', 
      kelas:'calon pegawai IT', 
      materi:'kaizen', 
      program:'BIT5', 
      trainer_name: 'Daniel'
    };
  }

  private _filter(value: String): String[] {
    const filterValue = value.toLowerCase();

    return this.trainer.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  ngOnInit() {
    this.trainerName = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  noEmpty(arr){
    for(var i = 0; i<arr.length; i++){
      if(arr[i] == "" || arr[i] == null){
        arr[i] = "-";
      }
    }
  }

  changeStartDate(event, target){
    var selected_date = this.dPipe.transform(event.value, 'dd MM yyyy');
    if(target){
      console.log("set start date");
      this.tempStartDate = selected_date;
    }else{
      console.log("set end date");
      this.tempEndDate = selected_date;
    }
  }

  toggleClass(){
    document.getElementById("btnLeft").classList.toggle('btn-active-green');
    document.getElementById("btnRight").classList.toggle('btn-active-red');
  }
  rightFunction(){
    if(this.flag){
      window.history.back();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
      this.toggleClass();
      this.myControl.disable();
    }
  }
  leftFunction(){
    if(this.flag){
      this.flag = false;
      this.leftIcon = "check";
      this.rightIcon = "close";
      this.toggleClass();
      this.myControl.enable();
    }//else{
    //  this.updateData();
    //}
  }
  //updateData(){
  //  console.log("saved!!");
    //api here
  //}

}
