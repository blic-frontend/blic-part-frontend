import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatDatepickerModule, 
          MatInputModule, 
          MatButtonModule, 
          MatIconModule, 
          MatAutocompleteModule, 
          MatNativeDateModule 
        } from '@angular/material';

import { ScheduleDetailRoutingModule } from './schedule-detail-routing.module';
import { ScheduleDetailComponent } from './schedule-detail.component';

@NgModule({
  imports: [
    CommonModule,
    ScheduleDetailRoutingModule,
    MatDatepickerModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule
  ],
  declarations: [ScheduleDetailComponent],
  providers: [DatePipe]
})
export class ScheduleDetailModule { }
