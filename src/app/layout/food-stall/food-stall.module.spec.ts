import { FoodStallModule } from './food-stall.module';

describe('FoodStallModule', () => {
  let foodStallModule: FoodStallModule;

  beforeEach(() => {
    foodStallModule = new FoodStallModule();
  });

  it('should create an instance', () => {
    expect(foodStallModule).toBeTruthy();
  });
});
