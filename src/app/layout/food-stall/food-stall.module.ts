import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatTooltipModule } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog'

import { FoodStallRoutingModule } from './food-stall-routing.module';
import { FoodStallComponent } from './food-stall.component';
import { ConfirmationDialog } from '../dialog/confirmation-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FoodStallRoutingModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule
  ],
  declarations: [FoodStallComponent, ConfirmationDialog],
  entryComponents: [ConfirmationDialog, FoodStallComponent]
})
export class FoodStallModule { }
