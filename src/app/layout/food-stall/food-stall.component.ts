import { Component, OnInit, Injectable } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Stall } from '../../services/stall';
import { MatDialog } from '@angular/material';
import { ConfirmationDialog } from '../dialog/confirmation-dialog.component';

@Component({
  selector: 'app-food-stall',
  templateUrl: './food-stall.component.html',
  styleUrls: ['./food-stall.component.scss']
})

@Injectable()
export class FoodStallComponent implements OnInit {
  allMenuArray: Stall[];
  presmananArray: Stall[];
  ugArray: Stall[];
  lgArray: Stall[];

  cal: Date;

  constructor(private _sanitizer: DomSanitizer, private dialog: MatDialog) {
    this.allMenuArray = [
      {id:1, location:'100', name:'presmanan', description:'ayam, perkedel, nasi, kerupuk, tempe, tahu, acar, timun, tomat, selada, kecap, es bruung walet, kue, soto', stock:200, queue_stock:300, reset_stock:300, soldout_counter:0, created_at:this.cal, created_by:'ds', updated_at:this.cal, updated_by:'jem', is_deleted:false},
      {id:2, location:'UG', name:'bento', description:'makanan', stock:200, queue_stock:300, reset_stock:300, soldout_counter:0, created_at:this.cal, created_by:'ds', updated_at:this.cal, updated_by:'jem', is_deleted:false},
      {id:3, location:'UG', name:'buntut', description:'makanan', stock:200, queue_stock:300, reset_stock:300, soldout_counter:0, created_at:this.cal, created_by:'ds', updated_at:this.cal, updated_by:'jem', is_deleted:false},
      {id:4, location:'LG', name:'bakmi', description:'makanan', stock:200, queue_stock:300, reset_stock:300, soldout_counter:0, created_at:this.cal, created_by:'ds', updated_at:this.cal, updated_by:'jem', is_deleted:false},
      {id:5, location:'LG', name:'ayam', description:'makanan', stock:200, queue_stock:300, reset_stock:300, soldout_counter:0, created_at:this.cal, created_by:'ds', updated_at:this.cal, updated_by:'jem', is_deleted:false},
      {id:6, location:'LG', name:'geprek', description:'makanan', stock:200, queue_stock:300, reset_stock:300, soldout_counter:0, created_at:this.cal, created_by:'ds', updated_at:this.cal, updated_by:'jem', is_deleted:false},
      {id:7, location:'UG', name:'sehat', description:'makanan', stock:200, queue_stock:300, reset_stock:300, soldout_counter:0, created_at:this.cal, created_by:'ds', updated_at:this.cal, updated_by:'jem', is_deleted:false},
      {id:8, location:'UG', name:'yakult', description:'makanan', stock:200, queue_stock:300, reset_stock:300, soldout_counter:0, created_at:this.cal, created_by:'ds', updated_at:this.cal, updated_by:'jem', is_deleted:false},
      {id:9, location:'LG', name:'rendang', description:'makanan', stock:200, queue_stock:300, reset_stock:300, soldout_counter:0, created_at:this.cal, created_by:'ds', updated_at:this.cal, updated_by:'jem', is_deleted:false},
      {id:10, location:'LG', name:'padang', description:'makanan', stock:200, queue_stock:300, reset_stock:300, soldout_counter:0, created_at:this.cal, created_by:'ds', updated_at:this.cal, updated_by:'jem', is_deleted:false},
    ]

    this.presmananArray = [];
    this.ugArray = [];
    this.lgArray = [];
  }

  getImg(img){
    return this._sanitizer.bypassSecurityTrustStyle(`url(${img})`);
  }

  bookingAction(data){
    console.log(data);
  }

  scScroll(target){
    target.scrollIntoView({behavior: 'smooth'});
  }
  clickTop(){document.getElementById("scTop").click()}
  clickMid(){document.getElementById("scMid").click()}
  clickBot(){document.getElementById("scBot").click()}

  ngOnInit() {
    this.allMenuArray.forEach(element => {
      if(element.location.includes('UG')){
        this.ugArray.push(element);
      }else if(element.location.includes('LG')){
        this.lgArray.push(element);
      }else{
        this.presmananArray.push(element);
      }
    });
  }

  openDialog(obj) {
    const dialogRef = this.dialog.open(ConfirmationDialog, {
      data: {
        food: obj.name
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}