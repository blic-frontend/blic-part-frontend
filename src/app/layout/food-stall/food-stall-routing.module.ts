import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FoodStallComponent } from './food-stall.component';

const routes: Routes = [
  {
    path: '',
    component: FoodStallComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoodStallRoutingModule { }
