export class Booking{
    booking_id: String;
    staff_id: String;
    stall_id: String;
    booking_time: Date;
}