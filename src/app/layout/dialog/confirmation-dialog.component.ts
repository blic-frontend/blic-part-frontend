import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

export class data{}

@Component({
    selector: 'confirmation-dialog',
    templateUrl: '../dialog/confirmation-dialog.component.html',
    styleUrls : ['../dialog/confirmation-dialog.component.scss']
  })
  
  export class ConfirmationDialog {
    constructor(@Inject(MAT_DIALOG_DATA) public obj: data) {}
  }