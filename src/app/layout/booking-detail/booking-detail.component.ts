import { Component, OnInit } from '@angular/core';

export class Model{
  booking_id: string;
  nip: string;
  name: string;
  program: string;
  stall_id: string;
  stall_name: string;
  stall_content: string;
  booking_time: string;
  rating: string;
}

@Component({
  selector: 'app-booking-detail',
  templateUrl: './booking-detail.component.html',
  styleUrls: ['./booking-detail.component.scss']
})

export class BookingDetailComponent implements OnInit {

  array1: string[];
  array2 : string[];
  obj: Model;
  rightIcon: string = "chevron_left";
  leftIcon: string = "create";
  flag: boolean = true;

  constructor() {
    this.obj = {
      booking_id:'b0001', 
      nip:'12345', 
      name:'yohanes', 
      program:'bit5', 
      stall_id:'s0001', 
      stall_name:'prasmanan', 
      stall_content:'ayam goreng, nasi, batagor, walet, naga, ikan, paya, kuecap, kwetiaw, soun, sop, cakwe, bubur, ikan, gajah, mamalia, cacing, uek, el  ', 
      booking_time:'20:15:10', 
      rating:'5'
    };
      
    this.array1 = Object.getOwnPropertyNames(this.obj);
    this.array2 = Object.values(this.obj);
  }

  ngOnInit() {
    this.noEmpty(this.array2);
  }

  noEmpty(arr){
    for(var i = 0; i<arr.length; i++){
      if(arr[i] == "" || arr[i] == null){
        arr[i] = "-";
      }
    }
  }

  rightFunction(){
    if(this.flag){
      window.history.back();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
    }
  }
  leftFunction(){
    if(this.flag){
      this.flag = false;
      this.leftIcon = "check";
      this.rightIcon = "close";
    }//else{
    //  this.updateData();
    //}
  }
  //updateData(){
  //  console.log("saved!!");
    //api here
  //}

}
