import { BookingDetailModule } from './booking-detail.module';

describe('BookingDetailModule', () => {
  let bookingDetailModule: BookingDetailModule;

  beforeEach(() => {
    bookingDetailModule = new BookingDetailModule();
  });

  it('should create an instance', () => {
    expect(bookingDetailModule).toBeTruthy();
  });
});
