import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatInputModule, MatIconModule } from '@angular/material';

import { BookingDetailRoutingModule } from './booking-detail-routing.module';
import { BookingDetailComponent } from './booking-detail.component';

@NgModule({
  imports: [
    CommonModule,
    BookingDetailRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule
  ],
  declarations: [BookingDetailComponent]
})
export class BookingDetailModule { }
