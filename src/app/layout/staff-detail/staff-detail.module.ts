import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatInputModule, MatIconModule, MatSelectModule} from '@angular/material';

import { StaffDetailRoutingModule } from './staff-detail-routing.module';
import { StaffDetailComponent } from './staff-detail.component';

@NgModule({
  imports: [
    CommonModule,
    StaffDetailRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule
  ],
  declarations: [StaffDetailComponent]
})
export class StaffDetailModule { }
