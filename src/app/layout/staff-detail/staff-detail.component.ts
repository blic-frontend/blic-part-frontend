import { Component, OnInit } from '@angular/core';

export class Model{
  nip: string;
  name: string;
  dob: string;
  gender: string;
  status: string;
  program: string;
  division: string;
  internal_eksternal: string;
}

@Component({
  selector: 'app-staff-detail',
  templateUrl: './staff-detail.component.html',
  styleUrls: ['./staff-detail.component.scss']
})
export class StaffDetailComponent implements OnInit {

  array1: string[];
  obj: Model;
  rightIcon: string = "chevron_left";
  leftIcon: string = "create";
  flag: boolean = true;
  program: string[];
  division: string[];
  editable: boolean = false;

  tempStatus: string;
  tempProgram: string;
  tempDivision: string;

  constructor() {
    this.obj = {
      nip:'12345', 
      name:'yohanes', 
      dob:'12-12-199x',
      program:'bit3', 
      gender:'male', 
      status:'trainee', 
      division:'gsit',
      internal_eksternal:'internal'
    };
      
    this.program = ['bit1', 'bit2', 'bit3', 'ppti', 'pba', 'asd', 'asdd', '2e', '231', 'asdaxz', 'xcz', 'po', 'kalkd', '182', 'akdhl', '12355']
    this.division = ['gsit', 'dpol', 'dpp']
  }

  ngOnInit() {
    this.tempStatus = this.obj.status;
    this.tempDivision = this.obj.division;
    this.tempProgram = this.obj.program;
  }

  noEmpty(arr){
    for(var i = 0; i<arr.length; i++){
      if(arr[i] == "" || arr[i] == null){
        arr[i] = "-";
      }
    }
  }

  toggleClass(){
    document.getElementById("btnLeft").classList.toggle('btn-active-green');
    document.getElementById("btnRight").classList.toggle('btn-active-red');
  }
  rightFunction(){
    if(this.flag){
      window.history.back();
    }else{
      this.rightIcon = "chevron_left";
      this.leftIcon = "create";
      this.flag = true;
      this.toggleClass();
    }
  }
  leftFunction(){
    if(this.flag){
      this.flag = false;
      this.leftIcon = "check";
      this.rightIcon = "close";
      this.toggleClass();
    }//else{
    //  this.updateData();
    //}
  }
  //updateData(){
  //  console.log("saved!!");
    //api here
  //}

}
