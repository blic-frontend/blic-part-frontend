import { Component, OnInit } from '@angular/core';

export interface PresmananObject{
  vendorId: string;
  foodId: string;
  foodLongText: string;
}

export interface StallObject{
  vendorId: string;
  stallId: string;
  stallName: string;
  stallPosition: string;
  stallMenu: string;
  stallBStock: number;
  stallQStock: number;
}

@Component({
  selector: 'app-dashboard-vendor',
  templateUrl: './dashboard-vendor.component.html',
  styleUrls: ['./dashboard-vendor.component.scss']
})

export class DashboardVendorComponent implements OnInit {

  presmananList: PresmananObject;
  stallList: StallObject;
  editFlag: number = 0;
  toggleText: string = "Edit";
  tempMenu: string = "";
  tempBStock: number = 0;
  tempQStock: number = 0;
  tempLongText: string = "";

  constructor() {
    this.presmananList = {
      vendorId:'v01',
      foodId:'v01',
      foodLongText:'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.'
    };

    this.stallList = {
      vendorId:'01',
      stallId:'s01',
      stallName:'Mie Ayam',
      stallPosition:'upperground',
      stallMenu:'Mie Ayam',
      stallBStock:50,
      stallQStock:100
    };
  }

  ngOnInit() {

  }

  navMenu(){document.getElementById("mat-tab-label-0-0").click()}
  saveMenu(){
    this.navMenu();
    this.stallList.stallMenu = this.tempMenu;
    this.stallList.stallBStock = this.tempBStock;
    this.stallList.stallQStock = this.tempQStock;
    this.presmananList.foodLongText = this.tempLongText;
    alert("Menu makanan telah di update!");
    document.getElementById("menuSbmt").setAttribute("disabled", "");
  }
  tabClick(){
    document.getElementById("menuSbmt").removeAttribute("disabled");
  }
}