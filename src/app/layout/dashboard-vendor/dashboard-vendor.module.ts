import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { 
  MatButtonModule, 
  MatCardModule, 
  MatIconModule, 
  MatTableModule, 
  MatFormFieldModule, 
  MatGridListModule,
  MatInputModule,
  MatTabsModule 
} from '@angular/material';

import { DashboardVendorRoutingModule } from './dashboard-vendor-routing.module';
import { DashboardVendorComponent } from './dashboard-vendor.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardVendorRoutingModule,
    MatGridListModule,
    MatCardModule,
    MatInputModule,
    FlexLayoutModule,
    MatCardModule,
    FormsModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatTabsModule
  ],
  declarations: [DashboardVendorComponent]
})
export class DashboardVendorModule { }
